# RSV pesmarca

Pesmarica [Rodu Sivega volka](www.rodsivegavolka.si).

## Navodila za zagon na lastnem računalniku

Za direktno sodelovanje pri urejanju pesmarice potrebuješ
[gitlab](https://gitlab.com/users/sign_up) račun.

### Linux

Potrebujete git, make, texlive-most, tekst editor (npr. [vim](www.vim.org)), pdf
bralec.

Za izdelavo pdf dokumentov pozeni v izvorni mapi repozitorija:

```
$ make all
```

### Windows

Za primer bomo uporabili [Visual Studio Code](https://code.visualstudio.com/),
ker je relativno razširjen in (meni) tudi vsaj približno poznan.

Najprej si naloži [Chocolatey](https://chocolatey.org/install#individual).

Ko imas Chocolatey inštaliran, v Power-shell-u, ki ga zaženeš kot skrbnik,
izvedeš ukaz:
```
choco install vscode git make strawberryperl miktex
```

V VSCode-u si naložiš še razširitev (extension) *Makefile Tools*.

Na tem mestu VSCode zapreš in ponovno odpreš. Greš na *Source Control -> Clone
Repository*. Odpre se ti okno, kamor prilepiš link:
```
https://gitlab.com/schrjako/rsv-pesmarca.git
```

Za prijavo izbereš *geslo* (*password*), kjer vpišeš svoje uporabniško ime in
geslo. VSCode bi moral nato na tvoj računalnik skopirati trenutno stanje
projekta (za katerega po želji določiš mesto, ki pa naj v poti nima šumnikov ali
presledkov).

Sedaj odpreš *view -> terminal*, kjer preizkusiš ukaz ('> ' je del, ki ti ga
terminal že sam napiše; make je edina stvar, ki jo moraš napisati):
```
> make
```
To bi ti moralo zgenerirati datoteki *pesmarica.pdf* in *pesmarica_akordi.pdf*.

Ko imaš odprt projekt klikneš na spodnje okno terminal in napišeš:
```
> git config --global user.name <tvoje ime>
> git config --global user.email <tvoj email>
```

Za uporabo git-a si lahko pogledš
[guide](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup).
Tudi če na koncu cele stvari ne boš delal v terminalu, so imena ista in je dobro
poznati idejo cele stvari. Za specifična vprašanja in težave pa se ne boj
kontaktirati koga, ki je morda bolj suveren od tebe.



## Navodila za pisanje pesmi z akordi

V *pesmarica.tex* je glavni opis pesmarice. Če si dodal novo pesem, jo vključiš
z `\include{pesmi/...}` po zgledu. Druge spremembe delaj, če veš kaj počneš.

Vsaka stvar od znaka procent (%) do konca vrstice je komentar. Če je tvoja pesem
že v seznamu pa je samo zakomentirana, jo lahko enostavno odkomentiraš tako, da
zbrišeš % pred `\include`.

Nove pesmi dodaš v mapi *pesmi/*. Ta datoteka naj bo sledeče oblike:
```
\begin{song}[remember-chords]{title={NASLOV},band={BAND},lyrics={LYRICS},composer={COMPOSER}}
...
\end{song}
```
Kjer namesto *NASLOV* napišeš naslov pesmi, ostale 3 vrednosti (*BAND*,
*LYRICS*, *COMPOSER*) pa so opcionalne, bi pa radi imeli vsaj eno. Ime datoteke
naj bo s sičniki in podčrtaji, *NASLOV* pa je s šumniki in vsem podobnim.
Namesto `...` dodaš kitice, ki so vsebovane med `\begin{kitica}` in
`\end{kitica}` ali `\begin{refren}` in `\end{refren}` za refren. Na koncu
vsakega verza sta dva backslasha (`\\`).

Če se del pesmi ponovi (npr. zadnja dva verza), ta del vmestiš med
`\begin{repeat}[N]` in `\end{repeat}`, kjer je `N` številka ponovitev. Zadnja
vrstica pred `\end{repeat}` naj nima na koncu dveh backslashov (`\\`).

Kako pisati akorde?
Durovi akordi naj bodo velike tiskane črke. G pomeni G-dur \\
Molovi akordi naj bodo velike tiskane črke in zraven mali tiskani m. Gm pomeni G-mol \\
Durovi in molovi septakordi imajo dodane sedmice. G7 pomeni G-durov septakord. Gm7 pomeni G-molov septakord. \\ 
Višaje pišemo s #, nižaje z b. C#m pomeni Cis-mol, Eb pomeni Es-dur. \\
B pomeni B-dur, H pomeni H dur. Če prepisuješ akorde iz angleško govorečih strani, moraš biti tu pozoren, ker oni uporabljajo B (naš H) in Bb (naš B). Zmeda? Ja. Vprašej Dodota.  \\
To načeloma zadostuje za 98% pesmi v pesmaricah. Bolj kompleksni akordi so opisani spodaj. \\

Obrati akordov (akordi, ki v basu nimajo osnovnega tona) so zapisani tako: D/F# To pomeni, da igramo D-dur, hkrati pa v basu primemo ton fis (F#). \\ 
Durovi in molovi akordi z dodano seksto imajo dodane šestice. G6 pomeni G-dur in dodano 6. \\
Enako velja za G9, G11, G13 (je pa res, da se taki akordi pojavljajo zelo zelo redko). \\
Če imajo akordi kakšne dodatne posebnosti, te zapišeš v oklepaj poleg akorda. Gm7(b5) pomeni G-molov septakord z znižano kvinto. G7(b9,#11) pomeni G-durov septakord z znižano 9 in zvišano 11. \\
Zvečani akordi naj bodo velike tiskane črke in zraven +. G+ pomeni G-dur z zvečano kvinto. \\


Akorde dodaš kar v verz, nad katerim jih hočeš imeti in takoj pred črko, nad
katero ga želiš imeti. Torej če hočeš, da verz izgleda približno takole:
```
        C          G
Dviga plamen se iz ognja,
       G7        C
s taborišča našega,
```
zapišeš:
```
Dviga pl^{C}amen se iz ^{G}ognja, \\
s tabor^{G7}išča našeg^{C}a, \\
```
Namesto `^{C}` lahko pišeš tudi `\chord{C}`. Za primer glej
`pesmi/dviga_plamen.tex`. Kjer je akord enočrkoven ga lahko pišeš brez zavitih
oklepajev ({}), drugod pa so zaviti oklepaji obvezni.

Ko se akordi v kasnejših kiticah ponavljajo, lahko uporabimo funkcijo
remember-chords (iz opisa pesmi). V vsaki neprvi kitici lahko z znakom caret
(`^`) vstaviš istoležen akord iz prve kitice te oblike.

Če imamo v pesmi več kitic različnih oblik, te kitice označimo z
`\begin{kitica}[class=IME] ... \end{kitica}`, kjer si IME izmisliš po želji.
Vsakič, ko želiš potem te akorde napišeš ponovno isto (`class=IME`), za ostale
kitice pa npr. `class=IME2`. Primer tega je v _pesmi/American_Pie.tex_.

Če hočeš dodati kakšen akord več (kot jih je bilo shranjenih/v prvi kitici tega
tipa (`class`-a)) to narediš z `\chord`, naslednjega pa lahko
preskocis z `\myskipchord`. Primer tega je v _pesmi/American_Pie.tex_, kjer v
zadnji kitici dodamo D7 in preskočimo D (v bistvu s tem zamenjamo D z D7).

Če hočeš napisati samo akorde (npr. za kitarski solo) to narediš z ukazom
`\writechord{}` ali `_`. Primer tega najdeš v _pesmi/Sanje.tex_.

Seznam dodatnih ukazov in njihovih efektov:

	- `\snc` - smash next chord - širina naslednjega akorda se ne upošteva pri
		izrisu
	- `\scs` - smash chords - ignoriraj širino vseh akordov (po defaultu je to tako)
	- `\nscs` - no smash chords - ne ignoriraj širine nobenih akordov
	- `\ifprntch{A}{B}` - if print chords - če se akordi pišejo (v pesmarica_akordi
		in pesamrica_vsi_akordi) se zapiše A, sicer B
	- `\ifprntach{A}{B}` - if print all chords - če se pišejo vsi akordi (v
		pesmarica_vsi_akordi) se zapiše A, sicer B
	- `\disablechords` - od tu naprej ne izpisuj akordov

Vsi ti ukazi (kot večina drugih stvari) vpliva samo v notranjosti trenutnega
okolja. To pomeni, da bo npr `\disablechords`, ki se nahaja v kitici, izklopil
izpisovanje akordov samo do konca kitice (primer v
`pesmi/Breakfast_at_Tiffanys.tex`).


Če ta navodila ne specificirajo oblike pesmi, ki jo urejaš, glede tega
kontaktiraj Dodota ali Jakoba.
