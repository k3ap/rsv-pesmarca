.PHONY: all

fmt=pdf
odir=build
interaction=nonstopmode
flags:=-output-format=$(fmt) -output-directory=$(odir) -interaction=$(interaction)
musicrepeat=\providecommand{\repeattype}{music}

all: pesmarica_akordi.pdf pesmarica.pdf pesmarica_vsi_akordi.pdf

pesmarica.pdf: pesmarica.tex pesmi/* | build/pesmi
	latex $(flags) -jobname=pesmarica '\providecommand{\printchords}{false}\input{pesmarica.tex}'

pesmarica_akordi.pdf: pesmarica.tex pesmi/* | build/pesmi
	latex $(flags) -jobname=pesmarica_akordi '\input{pesmarica.tex}'

pesmarica_vsi_akordi.pdf: pesmarica.tex pesmi/* | build/pesmi
	latex $(flags) -jobname=pesmarica_vsi_akordi '\providecommand{\printallchords}{true}\input{pesmarica.tex}'

build/pesmi: | build
	mkdir "$@"

build:
	mkdir $@

clean:
	rm -rvf build
