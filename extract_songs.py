import argparse
import sys
import os
from tools import printsong

delimiters = [' – ', ' - ']


def extract_songs(args):
    if not os.path.exists(args.output):
        os.makedirs(args.output)
    if args.input == '-':
        lines = sys.stdin.readlines()
    else:
        with open(args.input) as f:
            lines = f.readlines()
    i = 0
    while i < len(lines):
        for j in range(min(args.number, len(lines)-i)):
            print(f"{j:3}: {lines[i+j]}", end='')
        ans = input('What is the title of the next song (at end type -2)? ')
        if ans == '-2':
            printsong(lines[i:], args.output, delimiters)
            i=len(lines)
            break
        for j in range(len(lines)-i):
            if lines[i+j].lower().startswith(ans.lower()):
                printsong(lines[i:i+j], args.output, delimiters)
                i+=j
                break
        else:
            print(f'Title {ans} not found!!')


def extract_songs_type_title(args):
    if not os.path.exists(args.output):
        os.makedirs(args.output)
    if args.input == '-':
        lines = sys.stdin.readlines()
    else:
        with open(args.input) as f:
            lines = f.readlines()
    i = 0
    while i < len(lines):
        for j in range(min(args.number, len(lines)-i)):
            print(f"{j:3}: {lines[i+j]}", end='')
        ans = int(input('Where does the next song start? '))
        if ans == -1:
            # too few lines printed
            break
        if ans == -2:
            printsong(lines[i:], args.output, delimiters)
            i = len(lines)
        if ans == 0:
            continue
        printsong(lines[i:i+ans], args.output, delimiters)
        i += ans


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--output", "-o", help="Name of output files", default="pesmi2")
    parser.add_argument("--input", "-i", help="Name of input text file", default="-")
    parser.add_argument("--number", "-n", type=int, help="Number of displayed lines", default=100)
    #parser.add_argument("--delimiter", "-d", help="Delimiter between the title and band name", default='')
    args = parser.parse_args()
    extract_songs(args)
