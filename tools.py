from math import ceil, log


# def custom_split(text):
#     print(text)
#     p = ceil(log(len(text)))
#     for j in range(p):
#         for i in range(len(text)):
#             print(i // (10 ** j) % 10, end='')
#         print()
#     l, r = map(int, input('Which part is in the middle'

def printsong(lines, output, delimiters):
    title = lines[0].strip()
    band = lines[1].strip()
    #if delimiters:
    #    for delimiter in delimiters:
    #        if delimiter in title:
    #            title, band = title.split(delimiter, maxsplit=1)
    #            break
    filename = title.replace(' ', '_').replace('/', '_').replace('__', '_') + '.tex'
    translate = {'ć': 'c', 'č': 'c', 'Č': 'C', 'Ć': 'C', 'š': 's',
                 'Š': 'S', 'ž': 'z', 'Ž': 'Z', 'đ': 'dz', 'Đ': 'DZ'}
    for ci, co in translate.items():
        filename = filename.replace(ci, co)
    with open(f"{output}.txt", "a") as outfiles:
        outfiles.write(f"\\include{{{output}/{filename}}}\n")
    with open(f"{output}/{filename}", 'w') as f:
        textitle = title.replace(',', '\\,')
        if band:
            f.write(f"\\begin{{song}}{{title={{{textitle}}}, band={{{band}}}}}\n\n")
        else:
            f.write(f"\\begin{{song}}{{title={{{textitle}}}}}\n\n")
        t = ''.join(lines[2:])
        t = t.strip().replace('\n\n', '@')
        for verse in t.split('@'):
            f.write('\\begin{verse}\n')
            verses = verse.strip().split('\n')
            if len(verses) >= 4 and str(verses[-2:]) == str(verses[-4:-2]):
                if len(verses) > 4:
                    f.write(' \\\\\n'.join(verses[:-4]) + ' \\\\\n')
                f.write('\\begin{repeat}[2]\n')
                f.write(' \\\\\n'.join(verses[-2:]) + '\n')
                f.write('\\end{repeat}\n')
            else:
                f.write(' \\\\\n'.join(verses) + '\n')
            f.write('\\end{verse}\n\n')
        f.write("\\end{song}\n")
