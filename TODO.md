# TODO

1. Properly articulate TODO

2. Complete descriptions of commands and examples to README.md

3. Create songs list to add

4. Properly configure the layout, a table of contents, front page et cetera

## Zbiranje pesmi

Najprej zberemo pesmi in jih polektoriramo. Ko so polektorirane jih zapisemo
v latex z akordi in vsem. Kasnejse spremembe se vse dogajajo direktno v .tex
file-ih.
